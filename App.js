import React, { useEffect, useState } from "react";
import AppRouter from "./src/screens/navigation/AppRouter";
import * as SplashScreen from "expo-splash-screen";
import {
  useFonts,
  NotoSansJP_100Thin,
  NotoSansJP_300Light,
  NotoSansJP_400Regular,
  NotoSansJP_500Medium,
  NotoSansJP_700Bold,
  NotoSansJP_900Black,
} from "@expo-google-fonts/noto-sans-jp";
import { firebase } from "@react-native-firebase/app";
import { setGlobal } from "reactn";
import LoadingScreen from "./src/components/shared/LoadingScreen";
import { useGlobal } from "reactn";
import {
  SuccessToast,
  ErrorToast,
  NeutralToast,
} from "./src/components/shared/CustomToast";
import Toast from "react-native-toast-message";

SplashScreen.preventAutoHideAsync();
setGlobal({
  isLoading: false,
  user: null,
});
export default function App() {
  const [appIsReady, setAppIsReady] = useState(false);
  const [isLoading, setIsLoading] = useGlobal("isLoading");

  let [fontsLoaded] = useFonts({
    NotoSansJP_100Thin,
    NotoSansJP_300Light,
    NotoSansJP_400Regular,
    NotoSansJP_500Medium,
    NotoSansJP_700Bold,
    NotoSansJP_900Black,
  });
  useEffect(() => {
    async function prepare() {
      try {
        await new Promise((resolve) => setTimeout(resolve, 1300));
      } catch (e) {
        console.warn(e);
      } finally {
        setAppIsReady(true);
      }
    }

    if (fontsLoaded) {
      prepare();
    }
  }, [fontsLoaded]);

  useEffect(() => {
    if (appIsReady) {
      SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  if (!appIsReady) {
    return null;
  }

  return (
    <>
      <AppRouter />
      <LoadingScreen isLoading={isLoading} />
      <Toast
        config={{
          success: SuccessToast,
          error: ErrorToast,
          neutral: NeutralToast,
        }}
      />
    </>
  );
}
