import React, { createContext, useContext, useState, useEffect } from "react";
import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import { onSnapshot } from "@react-native-firebase/firestore";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsubscribeAuth = auth().onAuthStateChanged(async (userAuth) => {
      if (userAuth) {
        const userDoc = await firestore()
          .collection("users")
          .doc(userAuth.uid)
          .get();
        if (userDoc.exists) {
          setUser({ ...userDoc.data(), id: userAuth.uid });
          setLoading(false);
        } else {
          setUser(null);
          setLoading(false);
        }
      } else {
        setUser(null);
        setLoading(false);
      }
    });

    // Écoute des changements en temps réel dans Firestore
    const unsubscribeFirestore =
      user &&
      onSnapshot(
        firestore().collection("users").doc(user.id),
        (docSnapshot) => {
          if (docSnapshot.exists) {
            // Mise à jour des données de l'utilisateur en cas de changement dans Firestore
            setUser({ ...docSnapshot.data(), id: user.id });
          }
        }
      );

    return () => {
      // Arrêter les écoutes lorsque le composant est démonté
      if (unsubscribeAuth) unsubscribeAuth();
      if (unsubscribeFirestore) unsubscribeFirestore();
    };
  }, [user]);

  return (
    <UserContext.Provider value={{ user, loading, setUser }}>
      {!loading && children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
