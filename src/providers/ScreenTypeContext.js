import React, { createContext, useState, useEffect } from "react";
import { Dimensions } from "react-native";

export const ScreenTypeContext = createContext();

export const ScreenTypeProvider = ({ children }) => {
  const [isTablet, setisTablet] = useState(false);

  useEffect(() => {
    const updateScreenType = () => {
      const { width, height } = Dimensions.get("window");
      setisTablet(height / width < 1.6);
    };

    const subscription = Dimensions.addEventListener(
      "change",
      updateScreenType
    );
    updateScreenType();

    return () => {
      if (subscription.remove) {
        subscription.remove();
      }
    };
  }, []);

  console.log(isTablet, "isSquareScreen");

  return (
    <ScreenTypeContext.Provider value={isTablet}>
      {children}
    </ScreenTypeContext.Provider>
  );
};
