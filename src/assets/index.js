// images
import logo from "../assets/images/Logo.png";
import Video from "../assets/images/Video.png";
import wait from "../assets/images/wait.png";
import BtnDownload from "../assets/images/BtnDownload.png";
import BtnShared from "../assets/images/BtnShared.png";
import BtnLike from "../assets/images/BtnLike.png";
import BtnLikeRed from "../assets/images/BtnLikeRed.png";
import ToastPerdu from "../assets/images/ToastPerdu.png";

// icons
import add from "../assets/icons/add.png";
import arrowleft from "../assets/icons/arrowleft.png";
import close from "../assets/icons/close.png";
import download from "../assets/icons/download.png";
import param from "../assets/icons/param.png";
import picture from "../assets/icons/picture.png";
import search from "../assets/icons/search.png";
import send from "../assets/icons/send.png";
import video from "../assets/icons/video.png";
import refresh from "../assets/icons/refresh.png";
import arrow from "../assets/icons/arrow.png";

export const images = {
  logo,
  Video,
  wait,
  BtnDownload,
  BtnShared,
  BtnLike,
  BtnLikeRed,
  ToastPerdu,
};

export const icons = {
  add,
  arrowleft,
  close,
  download,
  param,
  picture,
  search,
  send,
  video,
  refresh,
  arrow,
};
