// REGEX
export const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
export const passwordRegex = /^.{6,}$/;
export const pseudonymeRegex = /^.{1,}$/;

// Firebase
export const convertFirebaseDateToDate = (firebaseTimestamp) => {
  return firebaseTimestamp.toDate().toLocaleDateString("fr-FR", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
  });
};

// helpers
export const truncateText = (text, maxLength) => {
  if (text.length > maxLength) {
    return text.substring(0, maxLength) + "..";
  }
  return text;
};

export const createFavoritesTag = (favorites) => {
  if (!favorites || favorites.length === 0) return null;

  const lastAddedImage = favorites[favorites.length - 1];
  return {
    id: "Favoris",
    name: "Favoris",
    totalFiles: favorites.length,
    lastAddedFileUrl: lastAddedImage.url,
    data: favorites,
  };
};

export const createAllTag = (fileCount, lastImage) => {
  if (fileCount === 0) return null;

  return {
    id: "All",
    name: "All",
    totalFiles: fileCount,
    lastAddedFileUrl: lastImage ? lastImage.url : null,
    data: lastImage ? [lastImage] : [],
  };
};

export const normalizeString = (str) => {
  return str
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, ""); // Retire les accents
};
