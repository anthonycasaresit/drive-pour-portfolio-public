import * as FileSystem from "expo-file-system";
import * as MediaLibrary from "expo-media-library";
import Toast from "react-native-toast-message";
import * as Sharing from "expo-sharing";

export const downloadImage = async (currentImage) => {
  if (!currentImage) {
    Toast.show({
      type: "error",
      text1: "Les devs en carton la",
    });
    return;
  }

  const { status } = await MediaLibrary.requestPermissionsAsync();
  if (status !== "granted") {
    Toast.show({
      type: "error",
      text1: "Faut accepter les permissions proplay !",
    });
    return;
  }

  const uri = currentImage.url;
  const fileName = currentImage.name + ".jpg";
  const fileUri = FileSystem.documentDirectory + fileName;

  try {
    const { uri: localUri } = await FileSystem.downloadAsync(uri, fileUri);
    await MediaLibrary.createAssetAsync(localUri);
    Toast.show({
      type: "success",
      text1: "Ha ouai ct drole sa !",
    });
  } catch (error) {
    console.error("Erreur lors du téléchargement:", error);
    Toast.show({
      type: "error",
      text1: "Les devs en carton la",
    });
  }
};

export const shareImage = async (currentImage) => {
  if (!currentImage) {
    Toast.show({
      type: "error",
      text1: "Aucune image sélectionnée",
    });
    return;
  }

  const uri = currentImage.url;
  const fileName = currentImage.name + ".jpg";
  const fileUri = FileSystem.cacheDirectory + fileName;

  try {
    const { uri: localUri } = await FileSystem.downloadAsync(uri, fileUri);

    if (!(await Sharing.isAvailableAsync())) {
      Toast.show({
        type: "error",
        text1: "Le partage n'est pas disponible sur cet appareil",
      });
      return;
    }
    await Sharing.shareAsync(localUri);
  } catch (error) {
    console.error("Erreur lors du partage:", error);
    Toast.show({
      type: "error",
      text1: "Erreur lors du partage",
    });
  }
};
