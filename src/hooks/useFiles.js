import { useState, useEffect } from "react";
import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";

const PAGE_SIZE = 30;

const useFiles = (selectedTags) => {
  const [files, setFiles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [hasMoreData, setHasMoreData] = useState(true);

  useEffect(() => {
    const fetchAndFilterFiles = async () => {
      setLoading(true);
      try {
        const querySnapshot = await firestore().collection("files").get();
        let fetchedFiles = querySnapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));

        if (selectedTags.length > 0) {
          fetchedFiles = fetchedFiles.filter((file) =>
            selectedTags.every((tag) => file.tags.includes(tag))
          );
        }

        setFiles(fetchedFiles);
        setHasMoreData(fetchedFiles.length === PAGE_SIZE);
      } catch (error) {
        console.error("Erreur lors de la récupération des fichiers: ", error);
        Toast.show({
          type: "error",
          text1: "Oula, erreur lors de la récupération des fichiers",
        });
      } finally {
        setLoading(false);
      }
    };

    fetchAndFilterFiles();

    const unsubscribe = firestore()
      .collection("files")
      .onSnapshot(
        (querySnapshot) => {
          let updatedFiles = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));

          if (selectedTags.length > 0) {
            updatedFiles = updatedFiles.filter((file) =>
              selectedTags.every((tag) => file.tags.includes(tag))
            );
          }

          setFiles(updatedFiles);
          setHasMoreData(updatedFiles.length === PAGE_SIZE);
        },
        (error) => {
          console.error("Erreur lors de l'écoute des fichiers: ", error);
          Toast.show({
            type: "error",
            text1: "Problème de mise à jour des fichiers",
          });
        }
      );

    return () => unsubscribe();
  }, [selectedTags]);

  return { files, loading, hasMoreData };
};

export default useFiles;
