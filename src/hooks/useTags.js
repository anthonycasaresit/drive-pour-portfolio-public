import { useState, useEffect } from "react";
import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";
const useTags = () => {
  const [tags, setTags] = useState([]);
  const [loading, setLoading] = useState(false);

  const loadTags = async () => {
    setLoading(true);
    try {
      const snapshot = await firestore()
        .collection("tags")
        .orderBy("name")
        .get();
      const newTags = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));

      setTags(newTags);
    } catch (error) {
      console.error("Erreur lors du chargement des tags: ", error);
      Toast.show({
        type: "error",
        text1: "Erreur lors du chargement des tags",
      });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    loadTags();

    const unsubscribe = firestore()
      .collection("tags")
      .orderBy("name")
      .onSnapshot((snapshot) => {
        const newTags = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setTags(newTags);
      });

    return () => unsubscribe();
  }, []);
  return { tags, loading };
};

export default useTags;
