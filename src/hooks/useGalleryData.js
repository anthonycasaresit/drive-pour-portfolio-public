import { useState, useEffect } from "react";
import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";

const useGalleryData = (tagId, limit = 50) => {
  const [files, setFiles] = useState([]);
  const [displayedFiles, setDisplayedFiles] = useState([]);
  const [lastDoc, setLastDoc] = useState(null);
  const [loading, setLoading] = useState(false);

  const loadFiles = async (loadMore = false) => {
    if (tagId === null) {
      return;
    }

    try {
      setLoading(true);
      let query = firestore().collection("files");

      if (tagId !== "All") {
        query = query.where("tags", "array-contains", tagId);
      }

      query = query.orderBy("addBy.at", "desc").limit(limit);

      if (loadMore && lastDoc) {
        query = query.startAfter(lastDoc);
      }

      const querySnapshot = await query.get();
      const newFiles = querySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));

      if (tagId === "All") {
        setDisplayedFiles((prev) =>
          loadMore ? [...prev, ...newFiles] : newFiles
        );
      } else {
        setFiles((prevFiles) =>
          loadMore ? [...prevFiles, ...newFiles] : newFiles
        );
      }

      setLastDoc(querySnapshot.docs[querySnapshot.docs.length - 1]);
    } catch (error) {
      console.error(
        "Erreur lors de la récupération des fichiers: useGalleryData  ",
        error
      );
      Toast.show({
        type: "error",
        text1: "Problème de récupération des fichiers",
      });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    loadFiles();
  }, [tagId]);

  const loadMoreFiles = () => {
    if (!loading && lastDoc) {
      loadFiles(true);
    }
  };

  // Retourne les fichiers affichés pour le tag "All", sinon les fichiers filtrés
  return {
    files: tagId === "All" ? displayedFiles : files,
    loading,
    loadMoreFiles,
  };
};

export default useGalleryData;
