import { useState, useEffect, useContext, useRef } from "react";
import { createFavoritesTag, createAllTag } from "../helper/helper";
import { fetchFileCountAndLastImage } from "../firebase/File/file";
import firestore from "@react-native-firebase/firestore";
import { useUser } from "../providers/UserProvider";

const useFakeTags = () => {
  const [favoritesTag, setFavoritesTag] = useState(null);
  const [allTag, setAllTag] = useState(null);
  const { user } = useUser();
  const prevFavoritesRef = useRef();

  useEffect(() => {
    const currentFavorites = JSON.stringify(user?.favorites);
    if (prevFavoritesRef.current !== currentFavorites) {
      prevFavoritesRef.current = currentFavorites;
      const tag = createFavoritesTag(user.favorites);
      setFavoritesTag(tag);
      console.log("favoritesTag updated");
    }
  }, [user?.favorites]);

  useEffect(() => {
    const updateAllTag = async () => {
      const { fileCount, lastImage } = await fetchFileCountAndLastImage();
      const tag = createAllTag(fileCount, lastImage);
      setAllTag(tag);
    };

    updateAllTag();

    const unsubscribe = firestore()
      .collection("files")
      .orderBy("addBy.at", "desc")
      .onSnapshot(() => {
        updateAllTag();
      });

    return () => unsubscribe();
  }, []);

  return { favoritesTag, allTag };
};

export default useFakeTags;
