import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { ActivityIndicator, View } from "react-native";
import LoginScreen from "../auth/LoginScreen";
import HomeScreen from "../HomeScreen";
import WaitingForValidationScreen from "../auth/WaitingForValidationScreen";
import AddMediaScreen from "../addMedia/AddMediaScreen";
import GalleryScreen from "../gallery/GalleryScreen";
import ProfilScreen from "../settings/ProfilScreen";
import firebase from "@react-native-firebase/app";
import firestore from "@react-native-firebase/firestore";
import { UserProvider } from "../../providers/UserProvider";
import { ScreenTypeProvider } from "../../providers/ScreenTypeContext";
import SettingsSceen from "../settings/SettingsSceen";
import Palette from "../../styles/Palette";
import AppRankings from "../settings/AppRankings";

const Stack = createNativeStackNavigator();

function AppRouter() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState(null);
  const [isUserValidated, setIsUserValidated] = useState(false);

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        const userRef = firestore().collection("users").doc(user.uid);
        const doc = await userRef.get();
        if (doc.exists && doc.data().isValidated) {
          setIsUserValidated(true);
          setUser(user);
        } else {
          setIsUserValidated(false);
          setUser(user);
        }
      } else {
        setUser(null);
        setIsUserValidated(false);
      }
      if (initializing) setInitializing(false);
    });

    return unsubscribe;
  }, [initializing]);

  if (initializing) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: Palette.blue[1],
        }}
      >
        <ActivityIndicator size="large" color={Palette.green} />
      </View>
    );
  }

  return (
    <>
      <UserProvider>
        <ScreenTypeProvider>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName={
                !user
                  ? "Login"
                  : isUserValidated
                  ? "Home"
                  : "WaitingForValidation"
              }
              screenOptions={{ headerShown: false }}
            >
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="GalleryScreen" component={GalleryScreen} />
              <Stack.Screen
                name="WaitingForValidation"
                component={WaitingForValidationScreen}
              />
              <Stack.Screen name="AddMedia" component={AddMediaScreen} />
              <Stack.Screen name="Profil" component={ProfilScreen} />
              <Stack.Screen name="Settings" component={SettingsSceen} />
              <Stack.Screen name="AppRankings" component={AppRankings} />
            </Stack.Navigator>
          </NavigationContainer>
        </ScreenTypeProvider>
      </UserProvider>
    </>
  );
}

export default AppRouter;
