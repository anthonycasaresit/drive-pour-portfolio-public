import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  BackHandler,
} from "react-native";
import Palette from "../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import AddButton from "../components/shared/AddButton";
import HomeHeader from "../components/header/HomeHeader";
import { useNavigation } from "@react-navigation/native";
import ImageViewer from "react-native-image-zoom-viewer";
import GalleryHeader from "../components/header/GalleryHeader";
import ParamModal from "../components/shared/ParamModal";
import useTags from "../hooks/useTags";
import FastImage from "react-native-fast-image";
import { truncateText } from "../helper/helper";
import useFiles from "../hooks/useFiles";
import ImageLoader from "../components/shared/ImageLoader";
import BottomBar from "../components/bottom/BottomBar";
import useFakeTags from "../hooks/useFakeTags";
import { images } from "../assets";
import { normalizeString } from "../helper/helper";
import { ScreenTypeContext } from "../providers/ScreenTypeContext";
import { useBackButtonHandler } from "../hooks/useBackButtonHandler";

const HomeScreen = () => {
  const { tags, loading } = useTags();
  const { favoritesTag, allTag } = useFakeTags();
  const isTablet = useContext(ScreenTypeContext);

  const [searchText, setSearchText] = useState("");
  const navigation = useNavigation();
  const [isSearching, setIsSearching] = useState(false);
  const [selectedTags, setSelectedTags] = useState([]);
  const [filteredTags, setFilteredTags] = useState([]);
  const [isViewerVisible, setIsViewerVisible] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [selectedFile, setSelectedFile] = useState(null);
  const [isSettingsVisible, setIsSettingsVisible] = useState(false);
  const [displayedTags, setDisplayedTags] = useState([]);
  const [page, setPage] = useState(0);
  const PAGE_SIZE = 100;

  useBackButtonHandler(() => {
    if (isSearching && !isViewerVisible) {
      resetSearch();
      return true;
    } else if (isViewerVisible) {
      setIsViewerVisible(false);
      return true;
    }
  });

  const {
    files: filteredFiles,
    loading: filesLoading,
    hasMoreData,
  } = useFiles(selectedTags);

  useEffect(() => {
    setIsSearching(searchText.length > 0 || selectedTags.length > 0);
  }, [searchText, selectedTags]);

  useEffect(() => {
    if (searchText) {
      const normalizedSearchText = normalizeString(searchText);
      const exactMatches = tags
        .map((tag) => tag.name)
        .filter((name) => normalizeString(name) === normalizedSearchText);

      const partialMatches = tags
        .map((tag) => tag.name)
        .filter(
          (name) =>
            normalizeString(name).includes(normalizedSearchText) &&
            normalizeString(name) !== normalizedSearchText
        );

      setFilteredTags([...exactMatches, ...partialMatches].slice(0, 5));
    } else {
      setFilteredTags([]);
    }
  }, [searchText, tags]);

  useEffect(() => {
    let initialTags = [...tags];
    if (favoritesTag) {
      initialTags = [favoritesTag, ...initialTags];
    }
    if (allTag) {
      initialTags = [allTag, ...initialTags];
    }
    setDisplayedTags(initialTags.slice(0, PAGE_SIZE));
  }, [tags, favoritesTag, allTag]);

  const openImage = (index) => {
    setCurrentIndex(index);
    setIsViewerVisible(true);
    setSelectedFile(filteredFiles[index]);
  };

  const handleImageChange = (index) => {
    setCurrentIndex(index);
    setSelectedFile(filteredFiles[index]);
  };

  const loadMoreTags = () => {
    console.log("loadMoreTags: Appelé");
    if (
      displayedTags.length >=
      tags.length + (favoritesTag ? 1 : 0) + (allTag ? 1 : 0)
    ) {
      console.log("loadMoreTags: Aucun tag supplémentaire à charger");
      return;
    }
    const nextPage = page + 1;
    let nextTags = tags.slice(0, nextPage * PAGE_SIZE);
    if (favoritesTag) {
      nextTags = [favoritesTag, ...nextTags];
    }
    if (allTag) {
      nextTags = [allTag, ...nextTags];
    }
    setDisplayedTags(nextTags);
    setPage(nextPage);
    console.log("loadMoreTags: Tags chargés", {
      nextPage,
      nextTagsLength: nextTags.length,
    });
  };

  const resetSearch = () => {
    setSearchText("");
    setSelectedTags([]);
    setFilteredTags([]);
  };

  const renderFooter = () => {
    // En mode de recherche filtrée par tags
    if (isSearching) {
      return filesLoading && hasMoreData ? (
        <ActivityIndicator size="large" color={Palette.green} />
      ) : null;
    } else {
      // En train de charger les tags (pas en mode de recherche)
      return displayedTags.length < tags.length ? (
        <ActivityIndicator size="large" color={Palette.green} />
      ) : null;
    }
  };

  const renderItem = ({ item, index }) =>
    isSearching ? (
      <View style={styles.itemContainer}>
        <TouchableOpacity onPress={() => openImage(index)}>
          <ImageLoader style={styles.image} uri={item.url} />
        </TouchableOpacity>
      </View>
    ) : (
      <View style={styles.itemContainer}>
        <TouchableOpacity
          onPress={() => navigation.navigate("GalleryScreen", { tag: item })}
        >
          <FastImage
            source={{
              uri: item.lastAddedFileUrl,
              priority: FastImage.priority.normal,
            }}
            style={styles.image}
            defaultSource={images.ToastPerdu}
          />
          <Text
            style={[
              styles.tagName,
              {
                fontSize: isTablet
                  ? responsiveFontSize(1.5)
                  : responsiveFontSize(2),
              },
            ]}
          >
            {truncateText(item.name, 10)}
          </Text>
          <Text
            style={[
              styles.tagCount,
              {
                fontSize: isTablet
                  ? responsiveFontSize(1.3)
                  : responsiveFontSize(1.8),
                lineHeight: isTablet
                  ? responsiveHeight(3)
                  : responsiveHeight(2),
              },
            ]}
          >
            {item.totalFiles}
          </Text>
        </TouchableOpacity>
      </View>
    );

  return (
    <SafeAreaView style={styles.MainContainer}>
      {isViewerVisible ? (
        <GalleryHeader
          TagName={selectedFile}
          currentImage={selectedFile}
          isViewerVisible={isViewerVisible}
          setIsViewerVisible={setIsViewerVisible}
          isSettingsVisible={isSettingsVisible}
          setIsSettingsVisible={setIsSettingsVisible}
        />
      ) : (
        <HomeHeader
          searchText={searchText}
          setSearchText={setSearchText}
          isSearching={isSearching}
          selectedTags={selectedTags}
          setSelectedTags={setSelectedTags}
          filteredTags={filteredTags}
          setFilteredTags={setFilteredTags}
          resetSearch={resetSearch}
        />
      )}
      {isViewerVisible ? (
        <ImageViewer
          imageUrls={filteredFiles.map((file) => ({ url: file.url }))}
          index={currentIndex}
          onSwipeDown={() => setIsViewerVisible(false)}
          enableSwipeDown={true}
          renderIndicator={() => null}
          onChange={handleImageChange}
          saveToLocalByLongPress={false}
        />
      ) : (
        <FlatList
          data={isSearching ? filteredFiles : displayedTags}
          renderItem={renderItem}
          keyExtractor={(item, index) =>
            item?.id?.toString() || index.toString()
          }
          numColumns={3}
          onEndReached={loadMoreTags}
          onEndReachedThreshold={0.5}
          ListFooterComponent={renderFooter}
          contentContainerStyle={{
            ...styles.flatListStyle,
            paddingBottom: responsiveHeight(10),
          }}
        />
      )}
      {!isSearching && <AddButton />}
      {isViewerVisible && <BottomBar currentImage={selectedFile} />}
      {isSettingsVisible && (
        <ParamModal
          isViewerVisible={isViewerVisible}
          currentImage={selectedFile}
          setIsSettingsVisible={setIsSettingsVisible}
          isLastImage={filteredFiles.length === 1}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[3],
    justifyContent: "flex-start",
    flex: 1,
  },
  flatListStyle: {
    alignItems: "center",
  },
  itemContainer: {
    margin: responsiveWidth(2),
  },
  image: {
    width: responsiveWidth(27),
    height: responsiveWidth(27),
    borderRadius: responsiveWidth(5),
  },
  tagName: {
    marginTop: responsiveHeight(1),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
    lineHeight: responsiveHeight(4),
    color: Palette.White,
  },
  tagCount: {
    color: Palette.White,
    fontFamily: "NotoSansJP_400Regular",
    lineHeight: responsiveHeight(2),
    opacity: 0.7,
  },
  BtnDowload: {
    position: "absolute",
    bottom: 0,
    alignSelf: "center",
  },
});

export default HomeScreen;
