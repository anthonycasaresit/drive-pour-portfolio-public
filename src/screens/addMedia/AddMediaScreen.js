import React, { Children, useEffect, useState } from "react";
import { View, Text, StyleSheet, SafeAreaView } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import AddMediaHeader from "../../components/header/AddMediaHeader";
import AddMediaForm from "../../components/form/AddMediaForm";
import Toast from "react-native-toast-message";
import {
  uploadFileAndCreateDocument,
  updateFile,
} from "../../firebase/File/file";
import { useGlobal } from "reactn";
import firestore from "@react-native-firebase/firestore";
import useTags from "../../hooks/useTags";
import {
  successMessagesAddMediaScreen,
  shouldShowToast,
} from "../../helper/toastHelper";
import { useUser } from "../../providers/UserProvider";

const AddMediaScreen = ({ route }) => {
  const navigation = useNavigation();
  const { user } = useUser();
  const [isLoading, setIsLoading] = useGlobal("isLoading");

  const [selectedMedia, setSelectedMedia] = useState([]);
  const [name, setName] = useState("");
  const [Currenttags, setTags] = useState([]);

  const { tags, loading } = useTags();

  const handleUploadOrEdit = async () => {
    if (selectedMedia.length === 0) {
      navigation.navigate("Home");
      return;
    }

    setIsLoading(true);

    if (route.params?.file) {
      try {
        const fileId = route.params.file.id;
        const updateData = {
          name: name,
          tags: Currenttags,
        };

        await updateFile(fileId, updateData);

        Toast.show({
          type: "success",
          text1: "Ok Boommer",
        });

        navigation.navigate("Home");
      } catch (error) {
        console.error("Erreur lors de la mise à jour: ", error);
        Toast.show({
          type: "error",
          text1: "ha j'y arrive pas",
        });
      } finally {
        setIsLoading(false);
      }
    } else {
      const currentMedia = selectedMedia[0];

      const fileData = {
        name: name,
        type: currentMedia.type,
        addBy: {
          id: user.id,
          name: user.pseudonym,
          at: firestore.Timestamp.fromDate(new Date()),
        },
        url: currentMedia.uri,
        tags: Currenttags,
      };

      try {
        await uploadFileAndCreateDocument(fileData);

        if (shouldShowToast()) {
          const randomMessageIndex = Math.floor(
            Math.random() * successMessagesAddMediaScreen.length
          );
          const randomMessage =
            successMessagesAddMediaScreen[randomMessageIndex];

          Toast.show({
            type: "success",
            text1: randomMessage,
          });
        }

        setSelectedMedia(selectedMedia.slice(1));
        setName("");
        setTags([]);

        if (selectedMedia.length === 1) {
          navigation.navigate("Home");
        }
      } catch (error) {
        console.error("Erreur lors de l'upload: ", error);
        Toast.show({
          type: "error",
          text1: "Erreur lors de l'upload du fichier.",
        });
      } finally {
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    if (route.params?.file) {
      const { file } = route.params;
      setName(file.name);
      setSelectedMedia([{ uri: file.url, type: file.type }]);
      setTags(file.tags);
      console.log("file", file);
    } else {
      const pickMedia = async () => {
        const permissionResult =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (permissionResult.status !== "granted") {
          Toast.show({
            type: "error",
            text1: "Permission nécessaire pour accéder à la galerie.",
          });
          return;
        }

        const pickerResult = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: false,
          allowsMultipleSelection: true,
          quality: 1,
          selectionLimit: 20,
        });

        console.log("Résultat de la sélection :", pickerResult);

        if (!pickerResult.canceled) {
          console.log("Images sélectionnées :", pickerResult.assets);
          setSelectedMedia(pickerResult.assets);
        } else {
          navigation.goBack();
        }
      };
      pickMedia();
    }
  }, [route.params]);

  return (
    <SafeAreaView style={styles.MainContainer}>
      <AddMediaHeader
        name={name}
        setName={setName}
        setTags={setTags}
        onSend={handleUploadOrEdit}
      />
      <AddMediaForm
        media={selectedMedia}
        name={name}
        setName={setName}
        tags={Currenttags}
        setTags={setTags}
        tagsList={tags.map((tag) => tag.name)}
        isEditing={route.params?.file}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[3],
    flex: 1,
  },
});

export default AddMediaScreen;
