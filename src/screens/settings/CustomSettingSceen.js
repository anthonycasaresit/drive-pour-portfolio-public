import React from "react";
import {
  View,
  ActivityIndicator,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from "react-native";
import Palette from "../../styles/Palette";
import { responsiveWidth } from "react-native-responsive-dimensions";
import SettingsHeader from "../../components/header/SettingsHeader";

const CustomSettingSceen = ({ isLoading, children, headerTitle }) => {
  return (
    <SafeAreaView style={styles.MainContainer}>
      <SettingsHeader title={headerTitle} />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color={Palette.green} />
        </View>
      ) : (
        <ScrollView style={styles.contentContainer}>{children}</ScrollView>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[3],
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    padding: responsiveWidth(6),
  },
});

export default CustomSettingSceen;
