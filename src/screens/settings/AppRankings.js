import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { useGlobal } from "reactn";
import CustomSettingScreen from "./CustomSettingSceen";
import { getAllUsers } from "../../firebase/users/users";
import { useUser } from "../../providers/UserProvider";

const AppRankings = ({ route, navigation }) => {
  const [users, setUsers] = useState([]);
  const [sortedUsers, setSortedUsers] = useState([]);
  const [uploadRanking, setUploadRanking] = useState(true);
  const [globalIsLoading, setGlobalIsLoading] = useState(false);
  const { user } = useUser();

  useEffect(() => {
    const fetchData = async () => {
      setGlobalIsLoading(true);
      try {
        const fetchedUsers = await getAllUsers();
        setUsers(fetchedUsers);
      } catch (error) {
        console.error("Erreur lors de la récupération des utilisateurs", error);
      }
      setGlobalIsLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    const sortUsers = () => {
      const sorted = [...users].sort((a, b) => {
        if (uploadRanking) {
          return b.totalFileUploads - a.totalFileUploads;
        } else {
          return b.createdTag.length - a.createdTag.length;
        }
      });

      const rankedUsers = sorted.map((user, index, array) => {
        let rank = index + 1;

        if (index > 0) {
          const prevUser = array[index - 1];
          const isSameRank = uploadRanking
            ? user.totalFileUploads === prevUser.totalFileUploads
            : user.createdTag.length === prevUser.createdTag.length;

          if (isSameRank) {
            user.rank = prevUser.rank;
          } else {
            user.rank = rank;
          }
        } else {
          user.rank = rank;
        }
        return user;
      });

      setSortedUsers(rankedUsers);
    };

    sortUsers();
  }, [users, uploadRanking]);

  const renderItem = ({ item, index }) => {
    return (
      <View
        style={
          user?.id === item?.id
            ? styles.RankingContainer2
            : styles.RankingContainer
        }
      >
        <View style={styles.containerRank}>
          <Text style={[styles.headerTitleTrue2, { marginRight: 20 }]}>
            {item.rank}
          </Text>
          <Text style={styles.headerTitleTrue2}>{item.pseudonym}</Text>
        </View>
        <View style={styles.containerRank2}>
          <Text style={styles.headerTitleTrue2}>
            {uploadRanking ? item.totalFileUploads : item.createdTag.length}
          </Text>
        </View>
      </View>
    );
  };

  const renderHeader = (
    <>
      <View style={styles.headerContainer}>
        <TouchableOpacity
          onPress={() => setUploadRanking(true)}
          style={
            uploadRanking ? styles.headerTouchableTrue : styles.headerTouchable
          }
        >
          <Text
            style={uploadRanking ? styles.headerTitleTrue : styles.headerTitle}
          >
            Total upload
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            !uploadRanking ? styles.headerTouchableTrue : styles.headerTouchable
          }
          onPress={() => setUploadRanking(false)}
        >
          <Text
            style={!uploadRanking ? styles.headerTitleTrue : styles.headerTitle}
          >
            Tags créés
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );

  return (
    <CustomSettingScreen isLoading={globalIsLoading} headerTitle="Classement">
      {renderHeader}
      <FlatList
        data={sortedUsers}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </CustomSettingScreen>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    marginBottom: responsiveHeight(1.5),
  },
  headerTouchable: {
    width: responsiveWidth(40),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: Palette.blue[2],
    padding: 5,
  },
  headerTouchableTrue: {
    width: responsiveWidth(40),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: Palette.green,
    padding: 5,
  },
  headerTitle: {
    fontSize: responsiveFontSize(2),
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
    opacity: 0.7,
  },
  headerTitleTrue: {
    fontSize: responsiveFontSize(2),
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
  },
  RankingContainer: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    paddingHorizontal: 25,
    backgroundColor: Palette.blue[1],
    marginVertical: 10,
    borderRadius: 20,
  },
  RankingContainer2: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    paddingHorizontal: 25,
    backgroundColor: Palette.green,
    marginVertical: 10,
    borderRadius: 20,
  },
  containerRank: {
    width: "50%",
    flexDirection: "row",
  },
  containerRank2: {
    width: "50%",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: Palette.blue[2],
    borderRadius: 20,
  },
  headerTitleTrue2: {
    fontSize: responsiveFontSize(1.8),
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
  },
});

export default AppRankings;
