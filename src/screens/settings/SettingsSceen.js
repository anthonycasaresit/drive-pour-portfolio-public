import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from "react-native";
import FastImage from "react-native-fast-image";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import CustomSettingScreen from "./CustomSettingSceen";
import Button from "../../components/shared/Button";
import { ScreenTypeContext } from "../../providers/ScreenTypeContext";
import { icons } from "../../assets/index";
import auth from "@react-native-firebase/auth";
import Toast from "react-native-toast-message";
import { useBackButtonHandler } from "../../hooks/useBackButtonHandler";

const SettingsSceen = ({ route, navigation }) => {
  const isTablet = React.useContext(ScreenTypeContext);

  useBackButtonHandler(() => {
    navigation.goBack();
    return true;
  });
  const handleSignOut = async () => {
    try {
      await auth().signOut();
      navigation.reset({
        index: 0,
        routes: [{ name: "Login" }],
      });
    } catch (error) {
      console.log(error);
      Toast.show({
        type: "error",
        text1: "Ta pas le droit de me quitter",
      });
    }
  };

  const settingsContent = (
    <>
      <View
        style={{
          flex: 1,
          height: responsiveHeight(65),
        }}
      >
        <TouchableOpacity onPress={() => navigation.navigate("Profil")}>
          <View style={styles.ContainerChoice}>
            <Text style={styles.Text}>Mon profil </Text>
            <Image style={styles.iconStyle} source={icons.arrow} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate("AppRankings")}>
          <View style={styles.ContainerChoice}>
            <Text style={styles.Text}>Classement</Text>
            <Image style={styles.iconStyle} source={icons.arrow} />
          </View>
        </TouchableOpacity>
      </View>

      <Button
        title="Déconnexion"
        BorderBtn
        onPress={handleSignOut}
        titleStyle={{
          fontSize: isTablet
            ? responsiveFontSize(1.5)
            : responsiveFontSize(2.4),
          color: Palette.red,
        }}
        style={{
          borderColor: Palette.red,
        }}
      />
    </>
  );

  return (
    <CustomSettingScreen isLoading={false} headerTitle="Paramètres">
      {settingsContent}
    </CustomSettingScreen>
  );
};

const styles = StyleSheet.create({
  ContainerChoice: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: responsiveHeight(2),
    padding: responsiveWidth(2),
    paddingHorizontal: responsiveWidth(5),
    backgroundColor: Palette.blue[1],
    borderRadius: 12,
  },
  Text: {
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: responsiveFontSize(2.2),
    paddingHorizontal: responsiveWidth(5),
    width: responsiveWidth(70),
    paddingLeft: responsiveWidth(2),
  },
  iconStyle: {
    width: responsiveWidth(7),
    height: responsiveWidth(7),
  },
});

export default SettingsSceen;
