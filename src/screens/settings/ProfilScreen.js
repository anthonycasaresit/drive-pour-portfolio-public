import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import FastImage from "react-native-fast-image";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import firestore from "@react-native-firebase/firestore";
import { convertFirebaseDateToDate } from "../../helper/helper";
import { UploadMilestoneToast } from "../../helper/toastHelper";
import { useUser } from "../../providers/UserProvider";
import CustomSettingScreen from "./CustomSettingSceen";

const ProfilScreen = ({ route, navigation }) => {
  const [sortedTags, setSortedTags] = useState([]);
  const [recentImages, setRecentImages] = useState([]);
  const { user, loading } = useUser();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchSortedTags = () => {
      if (user?.createdTag) {
        const sorted = [...user.createdTag].sort((a, b) => a.localeCompare(b));
        setSortedTags(sorted);
      }
    };

    const fetchImages = async () => {
      const images = [];
      const lastThreeIds = user.recentUploads.slice(-3);
      for (const id of lastThreeIds) {
        const imageData = await firestore().collection("files").doc(id).get();
        if (imageData.exists) {
          images.push(imageData.data());
        }
      }
      setRecentImages(images);
    };

    const fetchData = async () => {
      setIsLoading(true);
      await fetchSortedTags();
      await fetchImages();
      setIsLoading(false);
    };

    if (user) {
      fetchData();
    }
  }, []);

  const profilContent = (
    <>
      <View style={styles.ViewText}>
        <Text style={styles.Text}>Pseudonyme : </Text>
        <Text style={styles.TextTitle}>{user?.pseudonym}</Text>
      </View>
      <View style={styles.ViewText}>
        <Text style={styles.Text}>Date de creation: </Text>
        {user?.creationDate && (
          <Text style={styles.TextTitle}>
            {convertFirebaseDateToDate(user?.creationDate)}
          </Text>
        )}
      </View>
      <View style={styles.ViewText}>
        <Text style={styles.Text}>Total de fichier ajouter : </Text>
        <Text style={styles.TextTitle}>{user?.totalFileUploads}</Text>
      </View>
      <Text style={styles.Text}>Ajoutée récemment : </Text>
      <FlatList
        data={[...recentImages].reverse()}
        renderItem={({ item }) => (
          <View>
            <FastImage
              style={styles.image}
              source={{ uri: item.url }}
              onError={(error) => console.log("Erreur de chargement", error)}
            />
          </View>
        )}
        keyExtractor={(item, index) => String(index)}
        horizontal
      />
      <Text style={styles.Text}>Tags créés ({sortedTags.length}) :</Text>
      <View style={styles.tagsContainer}>
        {sortedTags.map((tag, index) => (
          <View key={index} style={styles.tagView}>
            <Text style={styles.tag}>{tag}</Text>
          </View>
        ))}
      </View>
      <UploadMilestoneToast totalUploads={user?.totalFileUploads} />
    </>
  );

  return (
    <CustomSettingScreen
      isLoading={isLoading || loading}
      headerTitle="Mon Profil"
    >
      {profilContent}
    </CustomSettingScreen>
  );
};

const styles = StyleSheet.create({
  ViewText: {
    flexDirection: "row",
    alignContent: "center",
    marginBottom: responsiveHeight(2),
  },
  Text: {
    color: Palette.White,
    fontSize: responsiveFontSize(2),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },
  TextTitle: {
    color: Palette.White,
    opacity: 0.8,
    fontSize: responsiveFontSize(2),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },

  tagsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingBottom: responsiveHeight(10),
    marginTop: responsiveWidth(3),
  },
  tag: {
    backgroundColor: Palette.secondary,
    paddingHorizontal: 6,
    paddingVertical: 4,
    margin: 3,
    borderRadius: 10,
    color: Palette.White,
    fontSize: responsiveFontSize(1.6),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },
  tagView: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Palette.green,
    paddingHorizontal: 5,
    paddingVertical: 2,
    margin: responsiveWidth(1),
    marginTop: responsiveWidth(2),
    borderRadius: 20,
  },
  image: {
    width: responsiveWidth(25),
    height: responsiveWidth(25),
    borderRadius: 20,
    margin: 5,
    marginVertical: responsiveWidth(5),
  },
});

export default ProfilScreen;
