import React, { useState, useEffect } from "react";
import {
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  BackHandler,
} from "react-native";
import GalleryHeader from "../../components/header/GalleryHeader";
import ImageViewer from "react-native-image-zoom-viewer";
import { responsiveWidth } from "react-native-responsive-dimensions";
import Palette from "../../styles/Palette";
import ParamModal from "../../components/shared/ParamModal";
import useGalleryData from "../../hooks/useGalleryData";
import ImageLoader from "../../components/shared/ImageLoader";
import BottomBar from "../../components/bottom/BottomBar";
import { showCustomToast } from "../../helper/toastHelper";
import { useBackButtonHandler } from "../../hooks/useBackButtonHandler";

const GalleryScreen = ({ route, navigation }) => {
  const { tag } = route.params;

  const [isFavorite, setIsFavorite] = useState(tag.id === "Favoris");
  const [isAll, setIsAll] = useState(tag.id === "All");
  const [imageData, setImageData] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [currentImage, setCurrentImage] = useState(null);
  const [isViewerVisible, setIsViewerVisible] = useState(false);
  const [isSettingsVisible, setIsSettingsVisible] = useState(false);
  const [page, setPage] = useState(0);
  const PAGE_SIZE = 50;

  const { files, loading, loadMoreFiles } = useGalleryData(
    isFavorite ? null : tag.id,
    PAGE_SIZE
  );

  useBackButtonHandler(() => {
    if (isViewerVisible) {
      setIsViewerVisible(false);
      return true;
    }
  });
  useEffect(() => {
    showCustomToast(tag.id);
  }, [tag.id]);

  useEffect(() => {
    if (isFavorite) {
      setImageData(tag.data);
    } else {
      setImageData(files);
      if (page > 0) {
        loadMoreFiles();
      }
    }
  }, [tag, isFavorite, files, page]);

  const handleLoadMore = () => {
    if (!loading && !isFavorite) {
      setPage((prevPage) => prevPage + 1);
    }
  };

  const renderFooter = () => {
    return loading ? (
      <ActivityIndicator size="large" color={Palette.green} />
    ) : null;
  };

  const imageUrls = imageData.map((file) => ({ url: file.url }));

  const openImage = (index) => {
    setCurrentIndex(index);
    setIsViewerVisible(true);
    setCurrentImage(imageData[index]);
  };

  const closeViewer = () => {
    setIsViewerVisible(false);
  };

  const handleImageChange = (index) => {
    setCurrentIndex(index);
    setCurrentImage(imageData[index]);
  };

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => openImage(index)}>
        <View style={styles.fileContainer}>
          <ImageLoader style={styles.image} uri={item.url} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.mainContainer}>
      <GalleryHeader
        TagName={tag.name}
        currentImage={currentImage}
        isViewerVisible={isViewerVisible}
        setIsViewerVisible={setIsViewerVisible}
        isSettingsVisible={isSettingsVisible}
        setIsSettingsVisible={setIsSettingsVisible}
        isFavorite={isFavorite}
        isAll={isAll}
      />
      {isViewerVisible && currentImage ? (
        <ImageViewer
          imageUrls={imageUrls}
          index={currentIndex}
          onSwipeDown={closeViewer}
          enableSwipeDown={true}
          renderIndicator={() => null}
          onChange={handleImageChange}
          saveToLocalByLongPress={false}
        />
      ) : (
        <FlatList
          data={imageData}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          numColumns={3}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.7}
          ListFooterComponent={renderFooter}
          style={styles.list}
          contentContainerStyle={styles.contentContainer}
        />
      )}
      {isViewerVisible && <BottomBar currentImage={currentImage} />}
      {isSettingsVisible && tag && (
        <ParamModal
          isViewerVisible={isViewerVisible}
          tag={tag}
          currentImage={currentImage}
          setIsSettingsVisible={setIsSettingsVisible}
          isLastImage={imageData.length === 1}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Palette.blue[3],
  },
  contentContainer: {
    alignItems: "center",
  },
  fileContainer: {
    flex: 1,
    margin: responsiveWidth(1),
  },
  image: {
    width: responsiveWidth(27),
    height: responsiveWidth(27),
    borderRadius: responsiveWidth(5),
    margin: responsiveWidth(0.5),
  },
  list: {},
  BtnDowload: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
});

export default GalleryScreen;
