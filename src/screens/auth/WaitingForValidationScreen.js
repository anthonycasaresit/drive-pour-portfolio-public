import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { images } from "../../assets/index";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import firebase from "@react-native-firebase/app";
import { useNavigation } from "@react-navigation/native";

const WaitingForValidationScreen = () => {
  const navigation = useNavigation();
  const user = firebase.auth().currentUser;

  useEffect(() => {
    const unsubscribe = firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .onSnapshot((doc) => {
        const userData = doc.data();
        if (userData && userData.isValidated) {
          navigation.replace("Home");
        }
      });

    return () => unsubscribe();
  }, []);

  return (
    <View style={styles.container}>
      <Image source={images.wait} style={styles.image} />
      <Text style={styles.text}>En attente de validation...</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Palette.blue[3],
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
    color: Palette.White,
    position: "absolute",
    bottom: responsiveHeight(10),
  },
  image: {
    width: responsiveWidth(80),
    height: responsiveHeight(30),
    resizeMode: "contain",
  },
});

export default WaitingForValidationScreen;
