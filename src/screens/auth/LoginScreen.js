import React, { useState, useEffect, useContext } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Palette from "../../styles/Palette";
import { images } from "../../assets/index";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import AuthForm from "../../components/form/AuthForm";
import Button from "../../components/shared/Button";
import { signIn, signUp } from "../../firebase/auth/authFirebase";
import { useGlobal } from "reactn";
import Toast from "react-native-toast-message";
import { useNavigation } from "@react-navigation/native";
import { ScreenTypeContext } from "../../providers/ScreenTypeContext";
import firebase from "@react-native-firebase/app";
import { useUser } from "../../providers/UserProvider";

const LoginScreen = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [pseudonyme, setPseudonyme] = useState("");
  const [isRegister, setIsRegister] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [isPseudonymeValid, setIsPseudonymeValid] = useState(false);
  const [isPasswordForgotten, setIsPasswordForgotten] = useState(false);
  const [isLoading, setIsLoading] = useGlobal("isLoading");
  const { user, loading, setUser } = useUser();

  const navigation = useNavigation();
  const isTablet = useContext(ScreenTypeContext);

  const handleSignUp = async () => {
    if (isEmailValid && isPasswordValid && isPseudonymeValid) {
      await signUp(email, password, pseudonyme, setIsLoading, setUser);
      setIsRegister(false);
    } else {
      Toast.show({
        type: "error",
        text1: "Securité anti dozo, desolé !",
      });
    }
  };

  const handleSignIn = async () => {
    if (isEmailValid && isPasswordValid) {
      const userData = await signIn(email, password, setIsLoading);
      setUser(userData);
      if (userData && userData.isValidated) {
        navigation.replace("Home");
      } else if (userData && !userData.isValidated) {
        navigation.replace("WaitingForValidation");
      } else {
        Toast.show({ type: "error", text1: "Je te connais pas toi !" });
      }
    } else {
      Toast.show({
        type: "error",
        text1: "Mail ou MDP non validé par la street !",
      });
    }
  };

  const handleSendPasswordResetEmail = async () => {
    if (!email || !/\S+@\S+\.\S+/.test(email)) {
      Toast.show({
        type: "error",
        text1: "Mail non valide, je suis pas un pigeon !",
      });
      return;
    }

    try {
      await firebase.auth().sendPasswordResetEmail(email);
      Toast.show({
        type: "success",
        text1: "Tkt je gere, check tes mails !",
      });
      setIsPasswordForgotten(false);
    } catch (error) {
      Toast.show({
        type: "error",
        text1: "Flemme... appelle casi",
      });
    }
  };

  return (
    <View style={styles.MainContainer}>
      <View style={styles.TopContainer}>
        <Image
          source={images.logo}
          style={isTablet ? styles.logoFold : styles.logo}
          resizeMode="contain"
        />
      </View>
      {!isRegister && (
        <Text
          style={{
            color: Palette.White,
            fontSize: isTablet ? responsiveFontSize(2) : responsiveFontSize(4),
            fontFamily: "NotoSansJP_500Medium",
            fontWeight: "bold",
            marginBottom: isTablet ? responsiveHeight(2) : responsiveHeight(5),
          }}
        >
          {isPasswordForgotten ? "Mot de passe oublié" : "Wow les gars"}
        </Text>
      )}
      <AuthForm
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        pseudonyme={pseudonyme}
        setPseudonyme={setPseudonyme}
        isRegister={isRegister}
        isEmailValid={isEmailValid}
        isPasswordValid={isPasswordValid}
        isPseudonymeValid={isPseudonymeValid}
        setIsEmailValid={setIsEmailValid}
        setIsPasswordValid={setIsPasswordValid}
        setIsPseudonymeValid={setIsPseudonymeValid}
        isPasswordForgotten={isPasswordForgotten}
      />

      <TouchableOpacity onPress={() => setIsPasswordForgotten(true)}>
        {!isRegister && !isPasswordForgotten && (
          <Text style={styles.mdpFrogot}>Mot de passe oublié ?</Text>
        )}
      </TouchableOpacity>

      <View style={styles.BotContainer}>
        <View>
          {isPasswordForgotten ? (
            <>
              <Button
                title="Envoyer le mail"
                onPress={handleSendPasswordResetEmail}
                style={{ marginBottom: responsiveHeight(2) }}
                titleStyle={{
                  fontSize: isTablet
                    ? responsiveFontSize(1.5)
                    : responsiveFontSize(2.4),
                }}
              />
              <Button
                title="Retour"
                BorderBtn
                onPress={() => setIsPasswordForgotten(false)}
                titleStyle={{
                  fontSize: isTablet
                    ? responsiveFontSize(1.5)
                    : responsiveFontSize(2.4),
                }}
              />
            </>
          ) : (
            <>
              {!isRegister && (
                <>
                  <Button
                    title="Se connecter"
                    onPress={handleSignIn}
                    style={{ marginBottom: responsiveHeight(2) }}
                    titleStyle={{
                      fontSize: isTablet
                        ? responsiveFontSize(1.5)
                        : responsiveFontSize(2.4),
                    }}
                  />
                  <Button
                    title="S'inscrire"
                    BorderBtn
                    onPress={() => setIsRegister(true)}
                    titleStyle={{
                      fontSize: isTablet
                        ? responsiveFontSize(1.5)
                        : responsiveFontSize(2.4),
                    }}
                  />
                </>
              )}
              {isRegister && (
                <>
                  <Button
                    title="S'inscrire"
                    onPress={handleSignUp}
                    style={{ marginBottom: responsiveHeight(2) }}
                    titleStyle={{
                      fontSize: isTablet
                        ? responsiveFontSize(1.5)
                        : responsiveFontSize(2.4),
                    }}
                  />
                  <Button
                    title="Retour"
                    BorderBtn
                    onPress={() => setIsRegister(false)}
                    titleStyle={{
                      fontSize: isTablet
                        ? responsiveFontSize(1.5)
                        : responsiveFontSize(2.4),
                    }}
                  />
                </>
              )}
            </>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[3],
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: responsiveWidth(12),
  },
  TopContainer: {
    width: "100%",
    height: responsiveHeight(33),
    justifyContent: "center",
  },
  logo: {
    width: "100%",
    marginTop: responsiveHeight(5),
  },
  BotContainer: {
    width: "100%",
    marginTop: responsiveHeight(5),
    justifyContent: "flex-end",
  },
  mdpFrogot: {
    color: Palette.green,
    fontSize: responsiveFontSize(1.8),
    fontFamily: "NotoSansJP_500Medium",
    textAlign: "right",
    textDecorationLine: "underline",
    marginTop: responsiveHeight(1),
  },

  // Fold styles
  logoFold: {
    width: "70%",
    marginTop: responsiveHeight(3),
    alignSelf: "center",
  },
});

export default LoginScreen;
