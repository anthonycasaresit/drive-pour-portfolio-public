import auth from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";

export const signUp = async (email, password, pseudonym, setIsLoading) => {
  try {
    setIsLoading(true);
    const userCredential = await auth().createUserWithEmailAndPassword(
      email,
      password
    );
    const uid = userCredential.user.uid;

    const newUser = {
      pseudonym,
      isValidated: false,
      creationDate: firestore.FieldValue.serverTimestamp(),
      totalFileUploads: 0,
      createdTag: [],
      recentUploads: [],
    };

    await firestore().collection("users").doc(uid).set(newUser);
    await AsyncStorage.setItem("email", email);
    await AsyncStorage.setItem("password", password);
    setIsLoading(false);
    Toast.show({
      type: "success",
      text1: "C'est bien chien",
    });
    console.log("Utilisateur inscrit et ajouté à Firestore avec succès!");
  } catch (error) {
    setIsLoading(false);
    Toast.show({
      type: "error",
      text1: "Chat a pas géré sa",
    });
    console.log("Erreur lors de l'inscription de l'utilisateur: ", error);
  }
};

export const signIn = async (email, password, setIsLoading) => {
  try {
    setIsLoading(true);
    const userCredential = await auth().signInWithEmailAndPassword(
      email,
      password
    );
    const uid = userCredential.user.uid;
    const userDoc = await firestore().collection("users").doc(uid).get();

    if (userDoc.exists) {
      const userData = {
        ...userDoc.data(),
        id: uid,
      };

      await AsyncStorage.setItem("email", email);
      await AsyncStorage.setItem("password", password);
      setIsLoading(false);
      Toast.show({
        type: "success",
        text1: `Salut ${userData.pseudonym}`,
      });
      return userData;
    } else {
      Toast.show({ type: "error", text1: "Chat a pas géré sa" });
      setIsLoading(false);
      return null;
    }
  } catch (error) {
    console.log("Erreur lors de la connexion de l'utilisateur: ", error);
    setIsLoading(false);
    Toast.show({
      type: "error",
      text1: "Mail ou MDP non validé par la street !",
    });
    return null;
  }
};
