import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";

export const getTags = async () => {
  try {
    const tagsCollection = firestore().collection("tags");
    const snapshot = await tagsCollection.get();

    const tags = [];
    snapshot.forEach((doc) => {
      tags.push({ id: doc.id, ...doc.data() });
    });

    return tags;
  } catch (error) {
    console.error("Erreur lors de la récupération des tags: ", error);
    Toast.show({
      type: "error",
      text1: "Chat a pas géré sa",
    });
    throw error;
  }
};

export const getTagNames = async () => {
  try {
    const tagsCollection = firestore().collection("tags");
    const snapshot = await tagsCollection.get();

    const tagNames = snapshot.docs.map((doc) => doc.data().name).sort();

    return tagNames;
  } catch (error) {
    console.error("Erreur lors de la récupération des tags: ", error);
    Toast.show({
      type: "error",
      text1: "Bleargh...Argh!!",
    });
    throw error;
  }
};
