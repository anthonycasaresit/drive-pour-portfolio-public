import storage from "@react-native-firebase/storage";
import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";
import { deleteFileToast } from "../../helper/toastHelper";

export const uploadFileAndCreateDocument = async (fileData) => {
  if (!fileData) {
    throw new Error("Données du fichier manquantes");
  }

  const filename = fileData.url.split("/").pop();
  const fileRef = storage().ref(`files/${filename}`);

  await fileRef.putFile(fileData.url);
  fileData.url = await fileRef.getDownloadURL();

  await firestore().collection("files").add(fileData);
};

export const fetchFilesByTag = async (tagName) => {
  try {
    const querySnapshot = await firestore()
      .collection("files")
      .where("tags", "array-contains", tagName)
      .get();

    return querySnapshot.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
  } catch (error) {
    console.error("Erreur lors de la récupération des fichiers: ", error);
    Toast.show({
      type: "error",
      text1: "Zut, cindy a manger tes souvenirs",
    });
    return [];
  }
};

export const fetchFilesByName = async (searchTerm) => {
  try {
    const querySnapshot = await firestore().collection("files").get();

    const normalizeString = (str) => {
      return str
        .toLowerCase()
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "");
    };

    const normalizedSearchTerm = normalizeString(searchTerm);

    const filteredFiles = querySnapshot.docs
      .map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }))
      .filter((file) =>
        normalizeString(file.name).includes(normalizedSearchTerm)
      );

    return filteredFiles;
  } catch (error) {
    console.error("Erreur lors de la recherche des fichiers: ", error);
    Toast.show({
      type: "error",
      text1: "Problème de recherche des fichiers",
    });
    return [];
  }
};

export const deleteFile = async (currentImage) => {
  if (!currentImage || !currentImage.id || !currentImage.url) {
    throw new Error("Données de l'image manquantes pour la suppression");
  }

  try {
    await firestore().collection("files").doc(currentImage.id).delete();

    const storageRef = storage().refFromURL(currentImage.url);
    await storageRef.delete();
    const randomMessage =
      deleteFileToast[Math.floor(Math.random() * deleteFileToast.length)];
    Toast.show({
      type: "success",
      text1: randomMessage,
    });
  } catch (error) {
    console.error("Erreur lors de la suppression du fichier: ", error);
    Toast.show({
      type: "error",
      text1: "Non je l'aime bien moi",
    });
  }
};

export const updateFile = async (fileId, updateData) => {
  if (!fileId || !updateData) {
    throw new Error("Données manquantes pour la mise à jour du fichier");
  }

  try {
    await firestore().collection("files").doc(fileId).update(updateData);

    Toast.show({
      type: "success",
      text1: "Ok Boommer",
    });
  } catch (error) {
    console.error("Erreur lors de la mise à jour du fichier: ", error);
    Toast.show({
      type: "error",
      text1: "Touche pas à mon fichier",
    });
  }
};
export const fetchFileCountAndLastImage = async () => {
  try {
    const filesRef = firestore().collection("files");
    const snapshot = await filesRef.orderBy("addBy.at", "desc").get();

    const fileCount = snapshot.size;
    let lastImage = null;

    if (!snapshot.empty) {
      lastImage = {
        id: snapshot.docs[0].id,
        ...snapshot.docs[0].data(),
      };
    }

    return { fileCount, lastImage };
  } catch (error) {
    console.error("Erreur lors de la récupération des informations: ", error);
    return { fileCount: 0, lastImage: null };
  }
};
