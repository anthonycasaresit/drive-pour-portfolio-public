import firestore from "@react-native-firebase/firestore";
import Toast from "react-native-toast-message";

export const getUserById = async (uid) => {
  try {
    const userDoc = await firestore().collection("users").doc(uid).get();

    if (!userDoc.exists) {
      console.error("Utilisateur non trouvé: ", uid);
      Toast.show({
        type: "error",
        text1: "Utilisateur non trouvé",
      });
      return null;
    }

    return { id: userDoc.id, ...userDoc.data() };
  } catch (error) {
    console.error("Erreur lors de la récupération de l'utilisateur: ", error);
    Toast.show({
      type: "error",
      text1: "Erreur lors de la récupération de l'utilisateur",
    });
    throw error;
  }
};
export const addFavorite = async (userId, image) => {
  try {
    await firestore()
      .collection("users")
      .doc(userId)
      .update({
        favorites: firestore.FieldValue.arrayUnion(image),
      });
  } catch (error) {
    console.error("Erreur lors de l'ajout aux favoris: ", error);
    Toast.show({
      type: "error",
      text1: "Moi je l'aime pas",
    });
  }
};

export const removeFavorite = async (userId, image) => {
  try {
    await firestore()
      .collection("users")
      .doc(userId)
      .update({
        favorites: firestore.FieldValue.arrayRemove(image),
      });
  } catch (error) {
    console.error("Erreur lors de la suppression des favoris: ", error);
    Toast.show({
      type: "error",
      text1: "Si t'adore sa",
    });
  }
};

export const getAllUsers = async () => {
  try {
    const userQuerySnapshot = await firestore()
      .collection("users")
      .where("isValidated", "==", true)
      .get();
    const users = [];
    userQuerySnapshot.forEach((doc) => {
      users.push({ id: doc.id, ...doc.data() });
    });
    return users;
  } catch (error) {
    console.error("Erreur lors de la récupération des utilisateurs: ", error);
    Toast.show({
      type: "error",
      text1: "Erreur lors de la récupération des utilisateurs",
    });
    throw error;
  }
};
