const Palette = {
  White: "#D9D9D9",
  green: "#1C7B47",
  red: "#D62839",
  blue: {
    1: "#202737",
    2: "#161D2D",
    3: "#111828",
  },
};

export default Palette;
