import React, { useContext } from "react";
import { TouchableOpacity, Text, StyleSheet, Image, View } from "react-native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { images } from "../../assets";
import { downloadImage, shareImage } from "../../helper/fileHelper";
import { addFavorite, removeFavorite } from "../../firebase/users/users";
import { useUser } from "../../providers/UserProvider";

const BottomBar = ({ currentImage }) => {
  const { user, loading } = useUser();
  const isFavorite = user?.favorites?.some((fav) => fav.id === currentImage.id);

  const handleFavorite = () => {
    if (isFavorite) {
      removeFavorite(user.id, currentImage);
    } else {
      addFavorite(user.id, currentImage);
    }
  };

  return (
    <View style={styles.bottomBar}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => downloadImage(currentImage)}
      >
        <Image source={images.BtnDownload} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button}
        onPress={() => shareImage(currentImage)}
      >
        <Image source={images.BtnShared} style={styles.icon} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={handleFavorite}>
        <Image
          source={isFavorite ? images.BtnLikeRed : images.BtnLike}
          style={styles.icon}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomBar: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    alignItems: "center",
    height: responsiveHeight(15),
    paddingHorizontal: responsiveWidth(5),
  },
  button: {
    width: responsiveWidth(20),
    height: responsiveWidth(20),
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    width: responsiveWidth(20),
    height: responsiveWidth(20),
    resizeMode: "contain",
  },
});

export default BottomBar;
