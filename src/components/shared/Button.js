import React, { useState, useEffect, useContext } from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";

const Button = ({
  title = "",
  style = {},
  onPress = () => {},
  titleStyle = {},
  disabled = false,
  BorderBtn = false,
}) => (
  <TouchableOpacity
    style={[
      styles.button,
      BorderBtn && styles.BorderBtn,
      disabled && { backgroundColor: Palette.disabled },
      style,
    ]}
    onPress={onPress}
    disabled={disabled}
  >
    <Text
      style={[
        {
          fontSize: responsiveFontSize(2.4),
          fontFamily: "NotoSansJP_400Regular",
          color: BorderBtn ? Palette.green : "black",
        },
        titleStyle,
      ]}
    >
      {title}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    padding: 5,
    borderRadius: 12,
    width: responsiveWidth(70),
    backgroundColor: Palette.green,
    borderWidth: 1,
  },

  BorderBtn: {
    backgroundColor: "transparent",
    borderColor: Palette.green,
    borderWidth: 1,
  },
});

export default Button;
