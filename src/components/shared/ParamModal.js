import React, { useState, useEffect } from "react";
import Modal from "react-native-modal";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import Palette from "../../styles/Palette";
import { getUserById } from "../../firebase/users/users";
import { convertFirebaseDateToDate } from "../../helper/helper";
import { deleteFile } from "../../firebase/File/file";
import { useNavigation } from "@react-navigation/native";

const ParamModal = ({
  isViewerVisible,
  tag,
  currentImage,
  setIsSettingsVisible,
  isLastImage,
}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [pseudonym, setpseudonym] = useState("");
  const navigation = useNavigation();

  const handleOpenModal = () => {
    setModalVisible(true);
  };

  const handleCloseModal = () => {
    setModalVisible(false);
    setIsSettingsVisible(false);
  };

  const handleDelete = () => {
    setIsSettingsVisible(false);
    deleteFile(currentImage);
    if (isLastImage) {
      navigation.navigate("Home");
    }
  };

  useEffect(() => {
    const fetchUserData = async () => {
      if (tag?.created?.by) {
        const userData = await getUserById(tag.created.by);
        if (userData) {
          setpseudonym(userData.pseudonym);
        }
      }
    };

    fetchUserData();
  }, [tag]);

  const displayData = isViewerVisible ? currentImage : tag;
  const formattedDate = isViewerVisible
    ? convertFirebaseDateToDate(displayData.addBy.at)
    : convertFirebaseDateToDate(displayData.created.at);

  return (
    <View style={styles.modalBackground}>
      {isViewerVisible ? (
        <>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("AddMedia", { file: currentImage })
            }
          >
            <Text style={styles.Text0}>Modifier les Tags</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleOpenModal}>
            <Text style={styles.Text0}>Informations</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleDelete}>
            <Text style={[styles.Text0, { color: Palette.red }]}>
              Supprimer
            </Text>
          </TouchableOpacity>
        </>
      ) : (
        <>
          <TouchableOpacity onPress={handleOpenModal}>
            <Text style={styles.Text0}>Informations</Text>
          </TouchableOpacity>
        </>
      )}

      <Modal
        isVisible={isModalVisible}
        onBackdropPress={handleCloseModal}
        swipeDirection={["down"]}
        style={styles.modalView}
      >
        <View style={styles.contentContainer}>
          <Text style={styles.Title}>Informations</Text>
          <Text style={styles.TextTitle}>Nom</Text>
          <Text style={styles.Text}>{displayData.name}</Text>
          <Text style={styles.TextTitle}>Crée le</Text>
          <Text style={styles.Text}>{formattedDate}</Text>
          {isViewerVisible && (
            <>
              <Text style={styles.TextTitle}>Ajouté par</Text>
              <Text style={styles.Text}>{displayData.addBy?.name}</Text>
              <Text style={styles.TextTitle}>Tags</Text>
              <View style={styles.tagsContainer}>
                {displayData.tags.map((tag, index) => (
                  <View key={index} style={styles.tagView}>
                    <Text key={index} style={styles.tag}>
                      {tag}
                    </Text>
                  </View>
                ))}
              </View>
            </>
          )}
          {!isViewerVisible && (
            <>
              <Text style={styles.TextTitle}>Créé par</Text>
              <Text style={styles.Text}>{pseudonym}</Text>
              <Text style={styles.TextTitle}>Total de Fichiers</Text>
              <Text style={styles.Text}>{tag?.totalFiles}</Text>
            </>
          )}
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    backgroundColor: Palette.blue[1],
    padding: responsiveWidth(3),
    borderRadius: 20,
    width: responsiveWidth(45),
    position: "absolute",
    top: responsiveHeight(12),
    right: responsiveWidth(5),
  },
  Text0: {
    color: Palette.White,
    fontSize: responsiveFontSize(2.2),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
    lineHeight: responsiveHeight(2),
    marginVertical: responsiveHeight(2.5),
  },
  Text: {
    color: Palette.White,
    fontSize: responsiveFontSize(2),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
    lineHeight: responsiveHeight(2),
    marginVertical: responsiveHeight(0.8),
    marginBottom: responsiveHeight(2.5),
  },
  TextTitle: {
    color: Palette.White,
    opacity: 0.8,
    fontSize: responsiveFontSize(1.7),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },
  Title: {
    color: Palette.White,
    fontSize: responsiveFontSize(2.5),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
    marginBottom: responsiveHeight(3),
    alignSelf: "center",
  },
  handleIndicator: {
    width: 40,
    height: 8,
    borderRadius: 4,
    marginTop: 8,
  },
  modalView: {
    justifyContent: "flex-end",
    margin: 0,
  },
  contentContainer: {
    backgroundColor: Palette.blue[1],
    padding: responsiveWidth(6),
    borderRadius: 20,
    width: responsiveWidth(100),
    height: responsiveHeight(60),
    minHeight: responsiveHeight(60),
  },
  tagsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  tag: {
    backgroundColor: Palette.secondary,
    paddingHorizontal: 8,
    paddingVertical: 4,
    margin: 4,
    borderRadius: 10,
    color: Palette.White,
    fontSize: responsiveFontSize(1.8),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },
  tagView: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Palette.green,
    paddingHorizontal: 8,
    paddingVertical: 4,
    margin: responsiveWidth(1),
    marginTop: responsiveWidth(2),
    borderRadius: 20,
  },
});

export default ParamModal;
