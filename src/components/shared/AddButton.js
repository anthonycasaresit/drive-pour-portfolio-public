import React from "react";
import { TouchableOpacity, Image, StyleSheet } from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
} from "react-native-responsive-dimensions";
import { useNavigation } from "@react-navigation/native";
import * as ImagePicker from "expo-image-picker";
import { icons } from "../../assets/index";
import Toast from "react-native-toast-message";

const AddButton = () => {
  const navigation = useNavigation();

  const handlePress = () => {
    navigation.navigate("AddMedia");
  };

  return (
    <TouchableOpacity style={styles.button} onPress={handlePress}>
      <Image
        source={icons.add}
        style={{ width: responsiveWidth(20), height: responsiveWidth(20) }}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    position: "absolute",
    bottom: responsiveHeight(8),
    right: responsiveWidth(12),
  },
});

export default AddButton;
