import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import Palette from "../../styles/Palette";
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    width: responsiveWidth(22),
    height: responsiveWidth(22),
    borderRadius: 20,
    marginRight: responsiveWidth(1),
  },
  text: {
    color: "#000",
    flexWrap: "wrap",
    fontFamily: "NotoSansJP_500Medium",
  },
  MainContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});

const SuccessToast = ({ text1 }) => (
  <View style={[styles.MainContainer]}>
    <Image
      source={require("../../assets/images/ToastFun.png")}
      style={styles.image}
    />
    <View style={[styles.container, { backgroundColor: Palette.green }]}>
      <Text style={styles.text}>{text1}</Text>
    </View>
  </View>
);

const ErrorToast = ({ text1 }) => (
  <View style={[styles.MainContainer]}>
    <Image
      source={require("../../assets/images/ToastSad.png")}
      style={styles.image}
    />
    <View style={[styles.container, { backgroundColor: Palette.red }]}>
      <Text style={styles.text}>{text1}</Text>
    </View>
  </View>
);

const NeutralToast = ({ text1 }) => (
  <View style={[styles.MainContainer]}>
    <Image
      source={require("../../assets/images/ToastNormal.png")}
      style={styles.image}
    />
    <View style={[styles.container, { backgroundColor: Palette.green }]}>
      <Text style={styles.text}>{text1}</Text>
    </View>
  </View>
);

export { SuccessToast, ErrorToast, NeutralToast };
