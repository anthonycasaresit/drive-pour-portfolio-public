import React, { useState } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import FastImage from "react-native-fast-image";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
} from "react-native-responsive-dimensions";

const ImageLoader = ({ style, uri }) => {
  const [isLoaded, setIsLoaded] = useState(false);

  return (
    <View style={style}>
      <FastImage
        style={{
          width: responsiveWidth(27),
          height: responsiveWidth(27),
          borderRadius: responsiveWidth(5),
          margin: responsiveWidth(0.5),
        }}
        source={{ uri, priority: FastImage.priority.normal }}
        onLoad={() => setIsLoaded(true)}
      />
      {!isLoaded && (
        <ActivityIndicator
          style={StyleSheet.absoluteFill}
          size="small"
          color={Palette.green}
        />
      )}
    </View>
  );
};

export default ImageLoader;
