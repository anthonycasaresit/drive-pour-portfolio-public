import React, { useState, useContext } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  useWindowDimensions,
} from "react-native";
import Palette from "../../styles/Palette";
import { responsiveFontSize } from "react-native-responsive-dimensions";
import {
  emailRegex,
  pseudonymeRegex,
  passwordRegex,
} from "../../helper/helper";
import { ScreenTypeContext } from "../../providers/ScreenTypeContext";

const AuthForm = ({
  email,
  setEmail,
  password,
  setPassword,
  pseudonyme,
  setPseudonyme,
  isRegister,
  isEmailValid,
  isPasswordValid,
  isPseudonymeValid,
  setIsEmailValid,
  setIsPasswordValid,
  setIsPseudonymeValid,
  isPasswordForgotten,
}) => {
  const handleEmailChange = (text) => {
    setEmail(text);
    setIsEmailValid(emailRegex.test(text));
  };

  const handlePasswordChange = (text) => {
    setPassword(text);
    setIsPasswordValid(passwordRegex.test(text));
  };

  const handlePseudonymeChange = (text) => {
    setPseudonyme(text);
    setIsPseudonymeValid(pseudonymeRegex.test(text));
  };

  const isTablet = useContext(ScreenTypeContext);

  return (
    <View style={styles.formContainer}>
      <Text style={isTablet ? styles.inputTextFold : styles.inputText}>
        Email
      </Text>
      <TextInput
        style={[
          isTablet ? styles.inputFold : styles.input,
          isEmailValid && { borderBottomColor: Palette.green },
        ]}
        value={email}
        onChangeText={handleEmailChange}
        keyboardType="email-address"
      />

      {!isPasswordForgotten && (
        <>
          <Text style={isTablet ? styles.inputTextFold : styles.inputText}>
            Mot de passe
          </Text>

          <TextInput
            style={[
              isTablet ? styles.inputFold : styles.input,
              isPasswordValid && { borderBottomColor: Palette.green },
            ]}
            value={password}
            onChangeText={handlePasswordChange}
            secureTextEntry
          />
        </>
      )}
      {isRegister && (
        <>
          <Text style={isTablet ? styles.inputTextFold : styles.inputText}>
            Pseudonyme
          </Text>
          <TextInput
            style={[
              isTablet ? styles.inputFold : styles.input,
              isPseudonymeValid && { borderBottomColor: Palette.green },
            ]}
            value={pseudonyme}
            onChangeText={handlePseudonymeChange}
          />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  formContainer: {},
  inputText: {
    fontSize: responsiveFontSize(2),
    fontFamily: "NotoSansJP_400Regular",
    color: Palette.White,
    opacity: 0.8,
  },
  input: {
    color: Palette.White,
    fontSize: responsiveFontSize(2),
    borderBottomColor: Palette.White,
    borderBottomWidth: 1,
    opacity: 0.8,
  },

  // fold
  inputTextFold: {
    fontSize: responsiveFontSize(1.7),
    fontFamily: "NotoSansJP_400Regular",
    color: Palette.White,
    opacity: 0.8,
  },
  inputFold: {
    color: Palette.White,
    fontSize: responsiveFontSize(1.5),
    borderBottomColor: Palette.White,
    borderBottomWidth: 1,
    opacity: 0.8,
  },
});

export default AuthForm;
