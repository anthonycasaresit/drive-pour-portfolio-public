import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import Toast from "react-native-toast-message";

const AddMediaForm = ({
  media,
  name,
  setName,
  setTags,
  tags,
  tagsList,
  isEditing,
}) => {
  const [Tag, setTag] = useState("");
  const [filteredTags, setFilteredTags] = useState([]);

  const formatTag = (tag) => {
    return (
      tag.replace(/\s+/g, "").charAt(0).toUpperCase() +
      tag.slice(1).toLowerCase().replace(/\s+/g, "")
    );
  };

  const addTag = () => {
    const formattedTag = formatTag(Tag);

    if (formattedTag === "All" || formattedTag === "Favoris") {
      Toast.show({
        type: "error",
        text1: "L'ajout des tags 'All' ou 'Favoris' est interdit",
      });
    } else if (!tags.includes(formattedTag)) {
      setTags([...tags, formattedTag]);
      setName(name + formattedTag);
    }

    setTag("");
  };

  const normalizeString = (str) => {
    return str
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase();
  };

  useEffect(() => {
    if (Tag) {
      const filtered = tagsList
        .filter((t) => typeof t === "string")
        .filter((t) => normalizeString(t).startsWith(normalizeString(Tag)));
      setFilteredTags(filtered);
    } else {
      setFilteredTags([]);
    }
  }, [Tag, tagsList]);

  const selectTag = (tag) => {
    setFilteredTags([]);
    setTag(tag);
  };

  const selectedMedia = media?.assets ? media.assets[0] : null;
  const isButtonDisabled = Tag.length === 0;
  return (
    <View style={styles.MainContainer}>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollViewContainer}
      >
        {media.map((item, index) => (
          <Image
            key={index}
            source={{ uri: item.uri }}
            style={index === 0 ? styles.mainImage : styles.sideImage}
            resizeMode="contain"
          />
        ))}
      </ScrollView>
      <View style={styles.TextInputContainer}>
        <TextInput
          placeholder="Ajoutez des tags ...."
          placeholderTextColor={Palette.White}
          placeholderStyle={{
            fontFamily: "NotoSansJP_500Medium",
            opacity: 0.7,
          }}
          style={
            filteredTags.length > 0
              ? styles.TextInputContainerSugest
              : styles.TextInput
          }
          onChangeText={(text) => setTag(text)}
          value={Tag}
        />
        <TouchableOpacity
          style={[
            styles.Btn,
            isButtonDisabled ? styles.BtnDisabled : styles.BtnEnabled,
          ]}
          disabled={isButtonDisabled}
          onPress={addTag}
        >
          <Text style={styles.TextBtn}>Ajouter tag</Text>
        </TouchableOpacity>
      </View>
      {filteredTags.length > 0 && (
        <View style={styles.SuggestionsContainer}>
          {filteredTags.map((tag, index) => (
            <TouchableOpacity
              key={index}
              style={styles.SuggestionItem}
              onPress={() => selectTag(tag)}
            >
              <Text style={styles.SuggestionText}>{tag}</Text>
            </TouchableOpacity>
          ))}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[3],
    paddingTop: responsiveHeight(2),
  },
  TextInput: {
    backgroundColor: Palette.blue[1],
    width: responsiveWidth(60),
    height: "100%",
    borderRadius: 20,
    padding: 10,
    paddingLeft: 15,
    marginBottom: 20,
    fontFamily: "NotoSansJP_500Medium",
    color: Palette.White,
    opacity: 0.7,
  },
  TextInputContainerSugest: {
    backgroundColor: Palette.blue[1],
    width: responsiveWidth(60),
    height: "100%",
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    padding: 10,
    paddingLeft: 15,
    marginBottom: 20,
    fontFamily: "NotoSansJP_500Medium",
    color: Palette.White,
    opacity: 0.7,
    borderBottomColor: "#232E40",
    borderBottomWidth: 3,
  },
  TextBtn: {
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
    opacity: 0.7,
  },
  TextInputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: responsiveHeight(8),
    paddingHorizontal: responsiveWidth(5),
  },
  Btn: {
    backgroundColor: Palette.blue[1],
    padding: 10,
    borderRadius: 20,
    marginLeft: 10,
    justifyContent: "center",
    fontFamily: "NotoSansJP_500Medium",
  },
  BtnEnabled: {
    backgroundColor: "#1C7B47",
  },
  BtnDisabled: {
    backgroundColor: Palette.blue[1],
  },
  SuggestionsContainer: {
    backgroundColor: Palette.blue[1],
    padding: 5,
    width: responsiveWidth(60),
    opacity: 0.7,
    borderTopStartRadius: 5,
    borderTopEndRadius: 5,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: responsiveWidth(5),
  },
  SuggestionItem: {
    padding: 10,
  },
  SuggestionText: {
    fontFamily: "NotoSansJP_500Medium",
    color: Palette.White,
  },
  scrollViewContainer: {
    height: responsiveHeight(30),
    marginBottom: responsiveHeight(2),
    paddingLeft: responsiveWidth(50) - responsiveWidth(55) / 2, // La moitié de la largeur de l'écran moins la moitié de la largeur de la première image
    paddingRight: responsiveWidth(5),
  },
  mainImage: {
    width: responsiveWidth(55),
    height: responsiveHeight(30),
    borderWidth: 3,
    borderColor: Palette.green,
    marginRight: responsiveWidth(5),
    resizeMode: "cover", // Testez avec cover
  },
  sideImage: {
    width: responsiveWidth(45),
    height: responsiveHeight(30),
    marginRight: responsiveWidth(5),
    opacity: 0.4,
    resizeMode: "cover", // Testez avec cover
  },
});

export default AddMediaForm;
