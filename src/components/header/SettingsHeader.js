import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { icons } from "../../assets";

const SettingsHeader = ({ title }) => {
  const navigation = useNavigation();

  return (
    <View style={styles.MainContainer}>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={icons.arrowleft}
          style={styles.iconStyle}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View>
        <Text numberOfLines={1} ellipsizeMode="head" style={styles.Text}>
          {title}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[2],
    alignItems: "center",
    paddingHorizontal: responsiveWidth(5),
    height: responsiveHeight(10),
    display: "flex",
    flexDirection: "row",
  },
  Text: {
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: responsiveFontSize(2.5),
    paddingHorizontal: responsiveWidth(5),
    width: responsiveWidth(70),
    paddingLeft: responsiveWidth(2),
  },
  iconStyle: {
    width: responsiveWidth(10),
    height: responsiveWidth(10),
  },
  iconStyle2: {
    width: responsiveWidth(15),
    height: responsiveWidth(15),
  },
  iconStyle3: {
    width: responsiveWidth(8),
    height: responsiveWidth(8),
  },
});

export default SettingsHeader;
