import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { icons } from "../../assets";

const AddMediaHeader = ({ name, setName, setTags, onSend }) => {
  const navigation = useNavigation();

  return (
    <View style={styles.MainContainer}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          source={icons.arrowleft}
          style={styles.iconStyle}
          resizeMode="contain"
        />
      </TouchableOpacity>
      {name.length > 0 && (
        <>
          <View style={styles.MidContainer}>
            <Text numberOfLines={1} ellipsizeMode="head" style={styles.Text}>
              {name}
            </Text>

            <TouchableOpacity
              onPress={() => {
                setName(""), setTags([]);
              }}
            >
              <Image
                source={icons.close}
                style={styles.iconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={onSend}>
            <Image
              source={icons.send}
              style={styles.iconStyleSend}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[2],
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: responsiveWidth(5),
    height: responsiveHeight(10),
    display: "flex",
    flexDirection: "row",
  },
  MidContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: responsiveWidth(60),
    backgroundColor: Palette.blue[1],
    borderRadius: 20,
    padding: responsiveWidth(1),
    paddingHorizontal: responsiveWidth(3),
  },
  iconStyle: {
    width: responsiveWidth(10),
    height: responsiveWidth(10),
  },
  iconStyleSend: {
    width: responsiveWidth(13),
    height: responsiveWidth(13),
  },
  Text: {
    color: Palette.White,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: responsiveFontSize(1.7),
    width: responsiveWidth(42),
    paddingRight: responsiveWidth(1),
  },
});

export default AddMediaHeader;
