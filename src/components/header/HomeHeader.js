import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import Palette from "../../styles/Palette";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { icons } from "../../assets";
import Toast from "react-native-toast-message";
import { ScreenTypeContext } from "../../providers/ScreenTypeContext";

const HomeHeader = ({
  searchText,
  setSearchText,
  isSearching,
  selectedTags,
  setSelectedTags,
  filteredTags,
  setFilteredTags,
  resetSearch,
}) => {
  const navigation = useNavigation();
  const isTablet = useContext(ScreenTypeContext);

  const handleSelectTag = (tag) => {
    if (!selectedTags.includes(tag)) {
      if (selectedTags.length < 4) {
        setSelectedTags([...selectedTags, tag]);
        setSearchText("");
        setFilteredTags([]);
      } else {
        setSearchText("");
        Toast.show({
          type: "error",
          text1: "C'est 4 maximum !",
        });
      }
    }
  };

  return (
    <>
      <View style={styles.MainContainer}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            flex: 1,
          }}
        >
          <TextInput
            style={[
              styles.Text,
              {
                borderBottomLeftRadius:
                  isSearching && searchText.length > 0 ? 0 : 20,
                borderBottomRightRadius:
                  isSearching && searchText.length > 0 ? 0 : 20,

                borderBottomColor:
                  isSearching && searchText.length > 0 ? "#232E40" : null,
                borderBottomWidth:
                  isSearching && searchText.length > 0 ? 3 : null,
                fontSize: isTablet
                  ? responsiveFontSize(1.2)
                  : responsiveFontSize(2.2),
                paddingHorizontal: isTablet
                  ? responsiveWidth(3)
                  : responsiveWidth(5),
              },
            ]}
            placeholder="Rechercher..."
            placeholderTextColor={Palette.White}
            value={searchText}
            onChangeText={setSearchText}
          />
          <TouchableOpacity
            onPress={() =>
              isSearching ? resetSearch() : navigation.navigate("Settings")
            }
          >
            <Image
              source={isSearching ? icons.refresh : icons.param}
              style={[
                styles.iconStyle,
                {
                  width: isTablet ? responsiveWidth(6) : responsiveWidth(8),
                  height: isTablet ? responsiveWidth(6) : responsiveWidth(8),
                },
              ]}
              resizeMode="contain"
            />
          </TouchableOpacity>
          {isSearching && filteredTags.length > 0 && (
            <View style={styles.SuggestionsContainer}>
              {filteredTags.map((tag, index) => (
                <TouchableOpacity
                  key={index}
                  style={styles.SuggestionItem}
                  onPress={() => handleSelectTag(tag)}
                >
                  <Text style={styles.SuggestionText}>{tag}</Text>
                </TouchableOpacity>
              ))}
            </View>
          )}
        </View>
      </View>
      {selectedTags.length > 0 && (
        <View style={styles.tagContainer}>
          {selectedTags.map((tag, index) => (
            <View key={index} style={styles.tagView}>
              <Text style={styles.tag}>{tag}</Text>
              <TouchableOpacity
                onPress={() =>
                  setSelectedTags(selectedTags.filter((item) => item !== tag))
                }
              >
                <Image
                  source={icons.close}
                  style={styles.iconStyleclose}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          ))}
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: Palette.blue[2],
    paddingHorizontal: responsiveWidth(5),
    minHeight: responsiveHeight(10),
    zIndex: 1,
  },
  Text: {
    color: Palette.White,
    backgroundColor: Palette.blue[1],
    fontFamily: "NotoSansJP_700Bold",
    width: responsiveWidth(76),
    height: responsiveHeight(6),
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    lineHeight: responsiveFontSize(3.5),
  },
  iconStyle: {
    width: responsiveWidth(8),
    height: responsiveWidth(8),
  },
  SuggestionsContainer: {
    position: "absolute",
    top: responsiveHeight(7.8),
    backgroundColor: Palette.blue[1],
    padding: 5,
    width: responsiveWidth(76),
    borderTopStartRadius: 5,
    borderTopEndRadius: 5,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  SuggestionItem: {
    padding: 10,
  },
  SuggestionText: {
    fontFamily: "NotoSansJP_500Medium",
    color: Palette.White,
  },
  tagView: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Palette.green,
    paddingHorizontal: 5,
    paddingVertical: 2,
    margin: responsiveWidth(1),
    marginTop: responsiveWidth(2),
    borderRadius: 20,
    flexDirection: "row",
  },
  tag: {
    backgroundColor: Palette.secondary,
    paddingHorizontal: 6,
    paddingVertical: 4,
    margin: 3,
    borderRadius: 10,
    color: Palette.White,
    fontSize: responsiveFontSize(1.6),
    fontFamily: "NotoSansJP_700Bold",
    fontWeight: "bold",
  },
  iconStyleclose: {
    width: responsiveWidth(5),
    height: responsiveWidth(5),
    marginLeft: responsiveWidth(1),
  },
  tagContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: Palette.blue[2],
    paddingHorizontal: responsiveWidth(5),
    paddingVertical: responsiveHeight(1),
  },

  // tel mora
});

export default HomeHeader;
