const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.syncDataOnFileChange = functions.firestore
  .document("files/{fileId}")
  .onWrite(async (change, context) => {
    const beforeData = change.before.exists ? change.before.data() : null;
    const afterData = change.after.exists ? change.after.data() : null;
    const fileId = context.params.fileId;

    // Fonction pour mettre à jour ou créer un tag
    async function updateOrCreateTag(
      tagName,
      increment,
      fileUrl,
      fileType,
      userId
    ) {
      const tagRef = admin.firestore().collection("tags").doc(tagName);
      const tagDoc = await tagRef.get();

      let updateData = {
        totalFiles: admin.firestore.FieldValue.increment(increment),
        lastAddedFileUrl: fileUrl,
      };

      if (fileType === "image") {
        updateData.totalImageFiles =
          admin.firestore.FieldValue.increment(increment);
      } else if (fileType === "video") {
        updateData.totalVideoFiles =
          admin.firestore.FieldValue.increment(increment);
      }

      if (tagDoc.exists) {
        await tagRef.update(updateData);
      } else {
        await tagRef.set({
          name: tagName,
          ...updateData,
          created: {
            at: admin.firestore.FieldValue.serverTimestamp(),
            by: userId,
          },
        });
        // Ajouter le nouveau tag au champ createdTag de l'utilisateur
        const userRef = admin.firestore().collection("users").doc(userId);
        await userRef.update({
          createdTag: admin.firestore.FieldValue.arrayUnion(tagName),
        });
      }
    }

    // Cas de création d'un fichier
    if (!beforeData && afterData) {
      const userId = afterData.addBy.id;
      const userRef = admin.firestore().collection("users").doc(userId);

      await userRef.update({
        totalFileUploads: admin.firestore.FieldValue.increment(1),
        recentUploads: admin.firestore.FieldValue.arrayUnion(fileId),
      });

      for (const tagName of afterData.tags) {
        await updateOrCreateTag(
          tagName,
          1,
          afterData.url,
          afterData.type,
          userId
        );
      }
    }

    // Cas de mise à jour d'un fichier
    else if (beforeData && afterData) {
      const addedTags = afterData.tags.filter(
        (tag) => !beforeData.tags.includes(tag)
      );
      const removedTags = beforeData.tags.filter(
        (tag) => !afterData.tags.includes(tag)
      );

      for (const tagName of addedTags) {
        await updateOrCreateTag(
          tagName,
          1,
          afterData.url,
          afterData.type,
          afterData.addBy.id
        );
      }

      for (const tagName of removedTags) {
        const tagRef = admin.firestore().collection("tags").doc(tagName);
        await tagRef.update({
          totalFiles: admin.firestore.FieldValue.increment(-1),
          [beforeData.type === "image" ? "totalImageFiles" : "totalVideoFiles"]:
            admin.firestore.FieldValue.increment(-1),
        });
      }
    }

    // Cas de suppression d'un fichier
    else if (beforeData && !afterData) {
      const userId = beforeData.addBy.id;
      const userRef = admin.firestore().collection("users").doc(userId);
      await userRef.update({
        totalFileUploads: admin.firestore.FieldValue.increment(-1),
        recentUploads: admin.firestore.FieldValue.arrayRemove(fileId),
      });

      for (const tagName of beforeData.tags) {
        const tagRef = admin.firestore().collection("tags").doc(tagName);
        await tagRef.update({
          totalFiles: admin.firestore.FieldValue.increment(-1),
          [beforeData.type === "image" ? "totalImageFiles" : "totalVideoFiles"]:
            admin.firestore.FieldValue.increment(-1),
        });
      }
    }
  });
