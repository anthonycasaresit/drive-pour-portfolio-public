const functions = require("firebase-functions");
const admin = require("firebase-admin");

// Assurez-vous que Firebase est initialisé
if (!admin.apps.length) {
  admin.initializeApp();
}

exports.deleteUnusedTag = functions.firestore
  .document("tags/{tagId}")
  .onUpdate(async (change, context) => {
    const afterData = change.after.data();

    // Vérifier si le totalFiles est tombé à zéro
    if (afterData.totalFiles === 0) {
      try {
        // Supprimer le tag de la base de données
        await change.after.ref.delete();
        console.log(
          `Tag ${context.params.tagId} supprimé car il n'est plus utilisé.`
        );

        // Mettre à jour les utilisateurs ayant créé ce tag
        const tagId = context.params.tagId;
        const usersRef = admin.firestore().collection("users");
        const snapshot = await usersRef
          .where("createdTag", "array-contains", tagId)
          .get();

        snapshot.forEach(async (doc) => {
          await doc.ref.update({
            createdTag: admin.firestore.FieldValue.arrayRemove(tagId),
          });
        });
      } catch (error) {
        console.error(
          "Erreur lors de la suppression du tag ou de la mise à jour des utilisateurs:",
          error
        );
      }
    }
  });
