const admin = require("firebase-admin");
const functions = require("firebase-functions");

const syncDataOnFileChange = require("./syncData");
const deleteUnusedTag = require("./deleteUnusedTag");

exports.syncDataOnFileChange = syncDataOnFileChange;
exports.deleteUnusedTag = deleteUnusedTag;
